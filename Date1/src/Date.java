public class Date {

    /*[0,11] -->  12 bits --> ano
      [12-15] --> 4 bits -->  mes
      [16-20] --> 5 bits -->  dia   sobran 11 bits*/


    private int nro;

    public Date(int nro) {
        this.nro = nro;
    }

    public int getAno() {
        int ano = nro >>> 20;
        return ano;
    }

    public int getMes() {
        int mes = (nro << 12) >>> 28;
        return mes;
    }

    public int getDia() {
        int dia = (nro << 16) >>> 27;
        return dia;
    }

    public void setAno(int ano) {
        if (ano >= 0 && ano < 4096) {
            int mask = 1048575; //0000 0000 0000 1111 1111 1111 1111 1111
            nro = (nro & mask) | (ano << 20); // 0000 0000 0000 XXXX XXXX XXXX XXXX XXXX | YYYY YYYY YYYY 0000 0000 0000 0000 0000 = YYYY YYYY YYYY XXXX XXXX XXXX XXXX XXXX
        } else {
            System.out.println("Ano no valido");
        }
    }

    public void setMes(int mes) {
        if (mes > 0 && mes < 13) {
            int mask=-983041; // 1111 1111 1111 0000 1111 1111 1111 1111
            nro=(nro & mask)|(mes<<16);
        } else {
            System.out.println("Mes invalido");
        }
    }

    public void setDia(int dia) {
        if (dia > 0 && dia < 32) {
            int mask=-63489; // 1111 1111 1111 1111 0000 0111 1111 1111
            nro=(nro & mask)|(dia<<11);
        } else {
            System.out.println("Dia invalido");
        }
    }

    @Override
    public String toString() {
        return getDia()+"/"+getMes()+"/"+getAno();
    }
}
