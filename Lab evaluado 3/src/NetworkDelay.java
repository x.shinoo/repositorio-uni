import java.util.*;

/*
Integrantes: Bruno Caro, Natalia Carrasco, Sebastián Fierro, Richard Ortiz, Matías Urrutia, Juan Tapia

Fuentes:
https://leetcode.com/problems/network-delay-time/discuss/2310813/Dijkstra's-Algorithm-Template-List-of-Problems
https://docs.oracle.com/javase/8/docs/api/java/util/Map.html
https://docs.oracle.com/javase/7/docs/api/java/util/Arrays.html
https://www.softwaretestinghelp.com/java-queue-interface/#Queue_Methods_In_Java
*/

public class NetworkDelay {
    /*
    El método ayuda a resolver el mayor tiempo que se demora en enviar una señal desde el nodo dado hasta el resto
    del grafo en su totalidad. Esto lo resuelve mediante la creación de un hashmap que almacena los datos de adyacencia
    de la red, para posteriormente con el uso de un arreglo y queue, ejecutar el algoritmo de Dijkstra para almacenar en
    el arreglo los tiempos de demora de la señal desde el nodo origen hasta el resto de estos, para finalmente retornar el
    valor más grande entre estos, o en caso de que no alcance a todos retornar -1.
     */
    public int networkDelayTime(int[][] times, int n, int k) {
        //Creación de un Hashmap que contenga la lista de adyacencia del grafo.
        Map<Integer, Map<Integer, Integer>> map = new HashMap<>();
        for(int[] time : times) {
            int start = time[0];
            int end = time[1];
            int weight = time[2];
            //Agrega el nodo inicial como llave del map, si este existe y el nodo final y tiempo dentro del map asignado a esta.
            map.putIfAbsent(start, new HashMap<>());
            map.get(start).put(end, weight);
        }

        //Creación de arreglo para registrar los tiempos de cada nodo, como parte de Dijkstra.
        int[] dis = new int[n+1];
        Arrays.fill(dis, Integer.MAX_VALUE);
        dis[k] = 0;
        //Creación de cola para registrar los nodos y el tiempo total de recepción de cada uno.
        Queue<int[]> queue = new LinkedList<>();
        queue.add(new int[]{k,0});

        //Algoritmo de Dijkstra para registrar en el arreglo los tiempos menores de cada nodo.
        while(!queue.isEmpty()) {
            //Extrae el arreglo de la cola y guarda el nodo y su tiempo por separado.
            int[] cur = queue.poll();
            int curNode = cur[0];
            int curWeight = cur[1];

            /*
            Revisa el Hashmap en busca de guardar el tiempo de los hashmap de curNode uno por uno mientras el valor de este
            sea una clave en el map.
             */
            for(int next : map.getOrDefault(curNode, new HashMap<>()).keySet()) {
                int nextweight = map.get(curNode).get(next);
            /*
            En caso de que la suma de los tiempos sea menor que el guardado en el arreglo dis, lo reemplaza
            y agrega a la cola un nuevo arreglo del nodo next y la suma para continuar su revisión en caso
            de que tenga otro camino.
            */
                if(curWeight + nextweight < dis[next]) {
                    dis[next] = curWeight + nextweight;
                    queue.add(new int[]{next, curWeight + nextweight});
                }
            }
        }

        //Encontrar el valor máximo de tiempo en el arreglo dis.
        int res = 0;
        for(int i=1; i<=n; i++) {
            if(dis[i] > res) {
                res = Math.max(res, dis[i]);
            }
        }
        /*
        Retorna el valor res si este no es igual al valor integer más alto, en caso contrario retornará -1,
        indicando que este posiblemente posee un nodo separado al origen, o que no todos pueden recibir la señal.
        */
        return res == Integer.MAX_VALUE ? -1 : res;
    }

}
