import java.util.Stack;

public class MaximumNestingDepthOfTheParentheses {
    public int maxDepth(String s) {
        int cont = 0;
        Stack<Integer> pila = new Stack<>();

        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(')
                pila.push(i);
            else if (s.charAt(i) == ')') {
                if (cont < pila.size())
                    cont = pila.size();
                pila.pop();
            }
        }
        return cont;
    }
}
