import java.util.Stack;

/**
 *
 * @author Martita
 *
 * Para m�s info, visitar https://es.stackoverflow.com/questions/26907/infijo-a-posfijo-en-java
 * Y http://informatica.uv.es/iiguia/AED/laboratorio/P6/pr_06_2005.html
 *
 */

public class Parsing {

    public static String fromInfijoToPostfijo(String infijo) {
        String postfijo="";
        Stack<Character> helper=new Stack<>();
        char[] infijoArr=infijo.toCharArray();

        for (int i = 0; i < infijo.length(); i++) {
            //Si el dato es un numero, se agrega al resultado
            if(isANumber(infijoArr[i])){
                postfijo+=infijoArr[i];
                //Si el dato es un '(', se agrega a la pila
            }else if(infijoArr[i]=='('){
                helper.push('(');
                //Si el dato es un ')', saca los valores ya almacenados en la pila y los agrega en el string de resultado
                // Sacará y agregará en el string hasta que el que quede en la pila sea un '('
            }else if(infijoArr[i]==')'){
                while (helper.peek()!='('){
                    postfijo+=helper.pop();
                }
                helper.pop();
                //Si el dato no es ni numero ni parentesis, quiere decir que es otro simbolo. Si es otro simbolo, si la pila está vacía agrega el dato
                //sino, se agrega primero a la pila el dato con mayor prioridad entre el dato del tope de la pila y el de infijoArr, y luego el de
                //menor prioridad
            }else if(!isANumber(infijoArr[i])){
                if(helper.size()==0){
                    helper.push(infijoArr[i]);
                }else {
                    if(prioridad(infijoArr[i])>prioridad(helper.peek())){
                        helper.push(infijoArr[i]);
                    }else{
                        postfijo+=helper.pop();
                        helper.push(infijoArr[i]);
                    }
                }
            }
        }
        //Se agregan todos los datos que queden en la pila al string que se retorna
        while (helper.empty()!=true){
            postfijo+=helper.pop();
        }
        return postfijo;
    }

    private static boolean isANumber(char symbol) {
        return symbol == '0' || symbol == '1' || symbol == '2' || symbol == '3' ||
                symbol == '4' || symbol == '5' || symbol == '6' || symbol == '7' ||
                symbol == '8' || symbol == '9';
    }

    private static int prioridad(char symbol) {
        if(symbol == '(') return 0;
        if(symbol == '+' || symbol == '-') return 1;
        if(symbol == '*' || symbol == '/') return 2;
        return -1;
    }


    public static ArbolExpresion getArbol(String postfijo) {
        Stack<ArbolExpresion> helper=new Stack<>();
        //Revisa cada caracter de la expresion, viendo si es un numero o no, para definir el padre y los hijos
        for (int i = 0; i < postfijo.length(); i++) {
            ArbolExpresion arbol=new ArbolExpresion(postfijo.charAt(i));
            if(isANumber(postfijo.charAt(i))){
                helper.push(arbol);
            }else {
                arbol.left = helper.pop();
                arbol.right = helper.pop();
                helper.push(arbol);
            }
        }
        //Retorna el tope de la pila
        return helper.pop();
    }

    public static int CalculaExpresion(ArbolExpresion arbol){
        if(arbol==null){
            return 0;
        }
        int resultRight = CalculaExpresion(arbol.right), resultado = 0;

        //Si symbol es un número, resultado será ese numero
        if(isANumber(arbol.symbol)){
            resultado = arbol.symbol;
			//Si es un operador, resultRight hará esa operación con el resultado de la operación
			//recursiva del hijo de la izquierda
        }else if(prioridad(arbol.symbol) == 1 || prioridad(arbol.symbol) == 2){
            switch (arbol.symbol){
                case '*': resultado = resultRight*CalculaExpresion(arbol.left);
                    break;
                case '+': resultado = resultRight+CalculaExpresion(arbol.left);
                    break;
                case '-': resultado = resultRight-CalculaExpresion(arbol.left);
                    break;
                case '/': resultado = resultRight/CalculaExpresion(arbol.left);
                    break;
            }
        }
        return resultado;
    }
}
