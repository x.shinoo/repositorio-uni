import java.util.LinkedList;
import java.util.Queue;

class RecentCounter {
    private Queue<Integer> counter;

    public RecentCounter() {
        counter = new LinkedList<>();
    }

    public int ping(int t) {
        counter.add(t);
        while(counter.peek() < t - 3000) {
            counter.poll();
        }
        return counter.size();
    }
}
