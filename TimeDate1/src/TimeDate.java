public class TimeDate {
    /*[0,11] -->  12 bits --> ano
      [12-15] --> 4 bits -->  mes
      [16-20] --> 5 bits -->  dia
      [21-25] --> 5 bits -->  hora
      [26-31] --> 6 bits -->  minuto */


    private int nro;

    public TimeDate(int nro) {
        this.nro = nro;
    }

    public int getAno() {
        int ano = nro >>> 20;
        return ano;
    }

    public int getMes() {
        int mes = (nro << 12) >>> 28;
        return mes;
    }

    public int getDia() {
        int dia = (nro << 16) >>> 27;
        return dia;
    }

    public int getHora(){
        int hora = (nro << 21) >>>27;
        return hora;
    }

    public int getMinuto(){
        int minuto = (nro << 26) >>>26;
        return minuto;
    }

    public void setAno(int ano) {
        if (ano >= 0 && ano < 4096) {
            int mask = 1048575; //0000 0000 0000 1111 1111 1111 1111 1111
            nro = (nro & mask) | (ano << 20); // 0000 0000 0000 XXXX XXXX XXXX XXXX XXXX | YYYY YYYY YYYY 0000 0000 0000 0000 0000 = YYYY YYYY YYYY XXXX XXXX XXXX XXXX XXXX
        } else {
            System.out.println("Ano no valido");
        }
    }

    public void setMes(int mes) {
        if (mes > 0 && mes < 13) {
            int mask=-983041; // 1111 1111 1111 0000 1111 1111 1111 1111
            nro=(nro & mask)|(mes<<16);
        } else {
            System.out.println("Mes invalido");
        }
    }

    public void setDia(int dia) {
        if (dia > 0 && dia < 32) {
            int mask=-63489; // 1111 1111 1111 1111 0000 0111 1111 1111
            nro=(nro & mask)|(dia<<11);
        } else {
            System.out.println("Dia invalido");
        }
    }

    public void setHora(int hora){
        if(hora>=0 && hora<24){
            int mask=-1985;// 1111 1111 1111 1111 1111 1000 0011 1111
            nro=(nro&mask)|(hora<<6);
        }else {
            System.out.println("Hora invalida");
        }
    }

    public void setMinuto(int minuto){
        if (minuto >= 0 && minuto < 60) {
            int mask=-64; // 1111 1111 1111 1111 1111 1111 1100 0000
            nro=(nro & mask)|(minuto);
        } else {
            System.out.println("Minuto invalido");
        }
    }

    @Override
    public String toString() {
        return getDia()+"/"+getMes()+"/"+getAno()+" "+getHora()+":"+getMinuto();
    }

    public boolean sameDate (TimeDate t){
        if(this.getAno()==t.getAno()){
            if(this.getMes()== t.getMes()){
                if(this.getDia()==t.getDia()){
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isBefore (TimeDate t){
        int nro1, nro2;
        nro1 = (getAno() << 9) | (getMes() << 5) | (getDia());
        nro2 = (t.getAno() << 9) | (t.getMes() << 5) | (t.getDia());
        return nro1 < nro2;

    }

    public boolean isAfter (TimeDate t){
        int nro1, nro2;
        nro1 = (getAno() << 9) | (getMes() << 5) | (getDia());
        nro2 = (t.getAno() << 9) | (t.getMes() << 5) | (t.getDia());
        return nro1 > nro2;
    }
}
