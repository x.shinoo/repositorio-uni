import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner teclado=new Scanner(System.in);
        int opcion,minN,horaN,diaN,mesN,anoN;
        TimeDate fecha1=new TimeDate(0);

        TimeDate fecha=new TimeDate(0);

        System.out.println("Ingrese el minuto de la fecha:");
        int nro=teclado.nextInt();
        fecha.setMinuto(nro);
        System.out.println("Ingrese la hora de la fecha:");
        nro=teclado.nextInt();
        fecha.setHora(nro);
        System.out.println("Ingrese el dia de la fecha:");
        nro=teclado.nextInt();
        fecha.setDia(nro);
        System.out.println("Ingrese el mes de la fecha:");
        nro=teclado.nextInt();
        fecha.setMes(nro);
        System.out.println("Ingrese el ano de la fecha:");
        nro=teclado.nextInt();
        fecha.setAno(nro);

        System.out.println("Minuto de la fecha: "+fecha.getMinuto());
        System.out.println("Hora de la fecha: "+fecha.getHora());
        System.out.println("Dia de la fecha: "+fecha.getDia());
        System.out.println("Mes de la fecha: "+fecha.getMes());
        System.out.println("Ano de la fecha: "+fecha.getAno());

        do{
            System.out.println("\n\n...::::MENU PRINCIPAL::::...");
            System.out.println("1. Obtener minuto de la fecha");
            System.out.println("2. Obtener hora de la fecha");
            System.out.println("3. Obtener dia de la fecha");
            System.out.println("4. Obtener mes de la fecha");
            System.out.println("5. Obtener ano de la fecha");
            System.out.println("6. Cambiar minuto de la fecha");
            System.out.println("7. Cambiar hora de la fecha");
            System.out.println("8. Cambiar dia de la fecha");
            System.out.println("9. Cambiar mes de la fecha");
            System.out.println("10. Cambiar ano de la fecha");
            System.out.println("11. Obtener fecha en formato DD/MM/YYYY HH:MM");
            System.out.println("12. Comparar si la fecha del sistema es igual a otra ingresada");
            System.out.println("13. Comparar si la fecha del sistema es anterior a otra ingresada");
            System.out.println("14. Comparar si la fecha del sistema es posterior a otra ingresada");
            System.out.println("0. Salir");

            System.out.println("Seleccione una opcion:");
            opcion=teclado.nextInt();


            switch (opcion){
                case 1-> System.out.println("Minuto de la fecha: "+fecha.getMinuto());
                case 2-> System.out.println("Hora de la fecha: "+fecha.getHora());
                case 3-> System.out.println("Dia de la fecha: "+fecha.getDia());
                case 4-> System.out.println("Mes de la fecha: "+fecha.getMes());
                case 5-> System.out.println("Ano de la fecha: "+fecha.getAno());
                case 6 -> {
                    System.out.println("Ingrese el minuto:");
                    minN=teclado.nextInt();
                    fecha.setMinuto(minN);
                }
                case 7 -> {
                    System.out.println("Ingrese la hora:");
                    horaN=teclado.nextInt();
                    fecha.setHora(horaN);
                }
                case 8 -> {
                    System.out.println("Ingrese el dia:");
                    diaN=teclado.nextInt();
                    fecha.setDia(diaN);
                }
                case 9 -> {
                    System.out.println("Ingrese el mes:");
                    mesN=teclado.nextInt();
                    fecha.setMes(mesN);
                }
                case 10 -> {
                    System.out.println("Ingrese el ano:");
                    anoN=teclado.nextInt();
                    fecha.setAno(anoN);
                }
                case 11-> System.out.println(fecha.toString());
                case 12-> {
                    System.out.println("Ingrese el minuto de la fecha a comparar:");
                    minN=teclado.nextInt();
                    fecha1.setMinuto(minN);
                    System.out.println("Ingrese la hora de la fecha a comparar:");
                    horaN=teclado.nextInt();
                    fecha1.setHora(horaN);
                    System.out.println("Ingrese el dia de la fecha a comparar:");
                    diaN=teclado.nextInt();
                    fecha1.setDia(diaN);
                    System.out.println("Ingrese el mes de la fecha a comparar:");
                    mesN=teclado.nextInt();
                    fecha1.setMes(mesN);
                    System.out.println("Ingrese el ano de la fecha a comparar:");
                    anoN=teclado.nextInt();
                    fecha1.setAno(anoN);

                    if(fecha.sameDate(fecha1)){
                        System.out.println("Las fechas son iguales");
                    }else{
                        System.out.println("Las fechas no son iguales");
                    }
                }
                case 13-> {
                    System.out.println("Ingrese el dia de la fecha a comparar:");
                    diaN=teclado.nextInt();
                    fecha1.setDia(diaN);
                    System.out.println("Ingrese el mes de la fecha a comparar:");
                    mesN=teclado.nextInt();
                    fecha1.setMes(mesN);
                    System.out.println("Ingrese el ano de la fecha a comparar:");
                    anoN=teclado.nextInt();
                    fecha1.setAno(anoN);

                    if(fecha.isBefore(fecha1)){
                        System.out.println("La fecha del sistema es anterior a la ingresada");
                    }else{
                        System.out.println("La fecha del sistema no es anterior a la ingresada");
                    }
                }
                case 14-> {
                    System.out.println("Ingrese el minuto de la fecha a comparar:");
                    minN=teclado.nextInt();
                    fecha1.setMinuto(minN);
                    System.out.println("Ingrese la hora de la fecha a comparar:");
                    horaN=teclado.nextInt();
                    fecha1.setHora(horaN);
                    System.out.println("Ingrese el dia de la fecha a comparar:");
                    diaN=teclado.nextInt();
                    fecha1.setDia(diaN);
                    System.out.println("Ingrese el mes de la fecha a comparar:");
                    mesN=teclado.nextInt();
                    fecha1.setMes(mesN);
                    System.out.println("Ingrese el ano de la fecha a comparar:");
                    anoN=teclado.nextInt();
                    fecha1.setAno(anoN);

                    if(fecha.isAfter(fecha1)){
                        System.out.println("La fecha del sistema es posterior a la ingresada");
                    }else{
                        System.out.println("La fecha del sistema no es posterior a la ingresada");
                    }
                }
                case 0-> System.out.println("Saliendo...");
                default -> System.out.println("Opcion invalida");
            }

        }while(opcion!=0);
    }
}
