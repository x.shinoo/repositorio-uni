public class Hashing {

    /* Preguntas:
    a. ¿Qué ocurre con las funciones de inserción, búsqueda y eliminación cuando el factor de carga es cercano a 1?
    Aumentaría el tiempo que se tardan en el proceso, ya que tendrían que hacer el salto varias veces, ya que la tabla estaría más llena.

    b. Describa los pasos a realizar para duplicar el tamaño de la tabla hash y reducir el factor de carga. ¿Para qué valor de alpha es necesario
    realizar este proceso?
    Este proceso se debería de realizar cuando el factor de carga sea cercano a 1, como por ejemplo 0.8, ya que esto indica que la tabla está
    llenandose o está ya casi llena.
    Primeramente habría que crear un arreglo donde se guardarían cada uno de los nodos actualmente almacenados, luego a nuestro T habría
    que asignarle otro arreglo con el doble del tamaño que el T original.
    */
    class Nodo{
        int key;
        public Nodo(int k) {
            key = k;
        }
    }

    Nodo [] T;

    public Hashing(int m) {
        T = new Nodo[m];
        for(int i = 0; i < m; i++) T[i] = null;
    }

    // Implementar
    // return true si fue posible insertar
    // false en otro caso
    public boolean insertar(int key) {
        //Creo una variable int para ir pasando por el arreglo
        int i=0;
        //Si el puesto donde debería ir la clave está ocupado, se inserta en otro puesto
        //seguirá haciendo el salto hasta encontrar un puesto desocupado
        while (i!=T.length) {
            if (T[h(key,i)] == null) {
                T[h(key,i)]=new Nodo(key);
                return true;
            }
            i++;
        }
        return false;
    }

    // Implementar
    // Si la clave no existe, return null
    public Nodo buscar(int key) {
        //Creo una variable int para ir pasando por el arreglo
        int i=0;
        //Preguntará si el dato almacenado en la posición donde debería estar
        //la key es igual a key, si es así se devuelve el nodo, sino, busca en otra
        //posicion
        while (i!=T.length) {
            if (T[h(key,i)].key == key) {
                return T[h(key,i)];
            }
            i++;
        }
        return null;
    }

    // Implementar
    // return true si fue posible eliminar
    // false en otro caso
    public boolean eliminar(int key) {
        //Creo una variable int para ir pasando por el arreglo
        int i=0;
        //Si en la posicion está la key, se eliminará, sino, busca en otra posicion
        while (i!=T.length) {
            if (T[h(key,i)].key == key) {
                T[h(key,i)]=null;
                return true;
            }
            i++;
        }
        return false;
    }

    public double factorDeCarga(){
        int contador=0;
        //recorre el arreglo y va contando los puestos que no sean null, lo que nos daría el size
        for (Nodo t:T) {
            if(t!=null){
                contador++;
            }
        }
        //Retorna el size dividido por el tamaño del arreglo
        return contador/T.length;
    }


    int h(int k, int i) {
        return (h1(k) + (i * h2(k))) % T.length;
    }

    int h1(int k) {
        return k % T.length;
    }

    int h2(int k) {
        return 1 + (k % (T.length-1));
    }

}
